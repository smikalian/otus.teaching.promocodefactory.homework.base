﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<EmployeeRequest> _employeeRequestRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,IRepository<EmployeeRequest> employeeRequestRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _employeeRequestRepository = employeeRequestRepository;
            _roleRepository = roleRepository;
        }
        

        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPost]
        public async Task<ActionResult> CreateEmployee(Employee employee)
        {
            var emloyees = await _employeeRepository.CreateAsync(employee);

            List<Role> roles = await _roleRepository.GetAllAsync();
            //Добавление несуществующей роли
            if (employee.Roles != null)
            {
                foreach (var role in employee.Roles)
                {
                    if (roles.FirstOrDefault(x => x.Id == role.Id) == null)
                    {
                        _roleRepository.CreateAsync(role);
                    }
                }
            }
            return await Task.FromResult(Ok());
        }
     // [HttpPost]
     // [Route("Test2")]
     //    public async Task<ActionResult> CreateEmployeeRequest(EmployeeRequest employee)
     //    {
     //        var employees = await _employeeRequestRepository.CreateAsync(employee);
     //        return await Task.FromResult(Ok());
     //    }
     [HttpPost]
     [Route("Test2")]
     public async Task<ActionResult> CreateEmployeeRequest(Employee employee)
     {
        // var employees = _employeeRepository.GetAllAsync();
         
         var employeeModel = new EmployeeRequest()
         {
             Id = employee.Id,
             Email = employee.Email,
             FirstName = employee.FirstName,
             LastName = employee.LastName
         };
         //_employeeRepository.CreateAsync();
         return await Task.FromResult(Ok());
     }


     [HttpPut]
     public async Task<ActionResult> UpdateById(Guid id, Employee employeeUpd)
     {
         var employees = await _employeeRepository.GetByIdAsync(id);
         employees.FirstName = employeeUpd.FirstName;
         employees.LastName = employeeUpd.LastName;
         employees.Email = employeeUpd.Email;
         employees.AppliedPromocodesCount = employeeUpd.AppliedPromocodesCount;

         return await Task.FromResult(Ok());
     }

        [HttpDelete]
        public ActionResult DeleteByIdAsync(Guid id)
        {
            var employees = _employeeRepository.DeleteByIdAsync(id);
            return Ok();
        }
    }
}