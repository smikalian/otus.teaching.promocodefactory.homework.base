namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class EmployeeRequest
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}