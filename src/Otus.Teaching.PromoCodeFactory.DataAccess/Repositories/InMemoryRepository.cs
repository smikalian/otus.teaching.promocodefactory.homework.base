﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
 
        private List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<List<T>> GetAllAsync()
        {
         
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<List<T>> CreateAsync(T entity)
        {
            
            
            if (Data.FirstOrDefault(x => x.Id == entity.Id)==null)
            {
                Data.Add(entity);  
            }
             

            return Task.FromResult(Data);
        }


       

        
        public Task<T> UpdateByIdAsync(Guid id, T _employee)
        {

            var employee = Data.FirstOrDefault(x => x.Id == id);
            
                         
            return null;
        }

        public  bool DeleteByIdAsync(Guid id)
        {
            var employee = Data.FirstOrDefault(x => x.Id == id);
            
            var result = Data.Remove(employee);
            return result;

        }
    }
}